package org.spring.springboot.controller;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spring.springboot.bo.AwemeCommentBo;
import org.spring.springboot.bo.AwemeCommentsResponse;
import org.spring.springboot.bo.BaseOutBo;
import org.spring.springboot.domain.AwemeUser;
import org.spring.springboot.domain.IndexRange;
import org.spring.springboot.service.AwemeCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

@RestController
@RequestMapping("/aweme/api")
public class AppCommentRestController {
	private Logger logger = LoggerFactory.getLogger(AppCommentRestController.class);
	@Value("${send.server.url}")
	private String url;
	
	@Autowired
	private AwemeCommentService awemeCommentService;	
	
	@RequestMapping(value="/comments/add",method=RequestMethod.POST)
	public BaseOutBo addComment(@RequestBody AwemeCommentsResponse commentsResponse){
		BaseOutBo out = new BaseOutBo();
		if(!CollectionUtils.isEmpty(commentsResponse.getComments())){
			
			List<AwemeCommentBo> comments = commentsResponse.getComments();
			for(AwemeCommentBo comment : comments){
				if(comment.getUser() != null){
					awemeCommentService.addComment(comment);
				}
			}
		}
		return out;
	}
	
	@RequestMapping(value="/users/add",method=RequestMethod.POST)
	public BaseOutBo addUser(@RequestBody List<String> users){
		BaseOutBo out = new BaseOutBo();
		if(!CollectionUtils.isEmpty(users)){
			logger.info("receive users:" + JSONObject.toJSONString(users));
		}
		return out;
	}
	
	@RequestMapping(value="/users/get_follow",method=RequestMethod.GET)
	public BaseOutBo getFollowUserList(@RequestParam(value="user_id",required=true) String curUserId,@RequestParam(value = "count",required=true, defaultValue="100") Integer count) {
		BaseOutBo out = new BaseOutBo();
		try {
			IndexRange range  = awemeCommentService.getAndUpdateFollowIndex(curUserId,count);
			
			List<AwemeUser> userList =  awemeCommentService.queryUserInRange(range);
			
			if(userList == null || userList.isEmpty()) {
				throw new RuntimeException("查询结果为空");
			}
			out.setMessage(userList);
		}catch(Exception e) {
			out.setRes(10);
			out.setMessage(e.getMessage());
		}
		return out;
	}
	@RequestMapping(value="/users/get_message",method=RequestMethod.GET)
	public BaseOutBo getMessageUserList(@RequestParam(value="user_id",required=true) String curUserId,@RequestParam(value = "count",required=true, defaultValue="100") Integer count) {
		BaseOutBo out = new BaseOutBo();
		try {
			IndexRange range  = awemeCommentService.getAndUpdateMessageIndex(curUserId,count);
			
			List<AwemeUser> userList =  awemeCommentService.queryUserInRange(range);
			
			if(userList == null || userList.isEmpty()) {
				throw new RuntimeException("查询结果为空");
			}
			out.setMessage(userList);
		}catch(Exception e) {
			out.setRes(10);
			out.setMessage(e.getMessage());
		}
		return out;
	}
}
