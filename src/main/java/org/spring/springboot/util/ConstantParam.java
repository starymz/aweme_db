package org.spring.springboot.util;

import java.util.HashMap;
import java.util.Map;

public class ConstantParam {
	public static String DEVICE_STATUS_GETTASK = "100";
	public static String DEVICE_STATUS_UPDATE = "100";
	
	public static int PKG_STATUS_NEW = 0;
	public static int PKG_STATUS_STOP = 99;
	
	public static int TASK_STATUS_NEW = 0;
	public static int TASK_STATUS_STOP = 88;
	public static int TASK_STATUS_NOACCOUNT = 200;
	//任务状态
	public static int TASK_INSTANCE_CREATED = 0;
	public static int TASK_INSTANCE_EXECUTING = 2;
	
	public static int TASK_INSTANCE_TIMEOUT = 99;
	public static int TASK_INSTANCE_STOP = 88;
	public static int TASK_INSTANCE_DOWNLOAD_FINISH = 10;
	public static int TASK_INSTANCE_ADD_COMMENT_SUCC = 11;
	public static int TASK_INSTANCE_ACCOUNT_ERROR = 6;
	public static int TASK_INSTANCE_KEYWORD_ERROR = 7;
	public static String TASK_TIMEOUT_INFO = "任务超时";
	
	//实例类型
	public static int INSTANCE_TYPE_APP_DOWNLOAD = 0;
	public static int INSTANCE_TYPE_APP_RANK = 1;	
	public static int INSTANCE_TYPE_SEARCH_COMMENT = 2;
	
	
	public static int CLIENT_TYPE_SIMULATOR = 0;
	public static int CLIENT_TYPE_PROTOCOL = 1;
	
	
	public static int PACKAGE_TYPE_TASK = 1;
	public static int PACKAGE_TYPE_APP_RANK = 2;
	public static int PACKAGE_TYPE_APP_UPDATE = 5;
	
	//任务分配方式
	public static int TASK_DISP_TYPE_ONETIME = 0;	
	public static int TASK_DISP_TYPE_AVG = 1;
	
	//评论状态
	public static int APP_COMMENT_NEW = 0;
	public static int APP_COMMENT_EXECUTING = 1;	
	public static int APP_COMMENT_FINISHING = 10;	
	
	public static String TASK_MANAGE_TYPE_COMMON = "common";
	public static String TASK_MANAGE_TYPE_ACCOUNT_DEVICE_MAPPING = "account_device_mapping";
	
	
	public static int PROTOCOL_ACCOUNT_ERROR = 101;
	public static int PROTOCOL_TASK_SUCC = 100;
	public static int PROTOCOL_TASK_UNKNOWN = 200;
	
	 public final static Map<Integer, String> STATUS_INFO_MAP = new HashMap<Integer, String>(){/**
		 * 
		 */
		private static final long serialVersionUID = -8878879095006868308L;
		{
			put(100 ,"TASK_RET_OK ");
			put(101,"TASK_ERR_ACCOUNT");
			put(102,"TASK_ERR_CAP_GET");
			put(103,"TASK_ERR_CAP_DATA");
			put(104,"TASK_ERR_CAP_WRONG");
			put(1000,"TASK_ERR_CLEAN");
			put(1001,"TASK_ERR_LOGIN");
			put(1002,"TASK_ERR_HOME");
			put(1003,"TASK_ERR_SEARCH_HINT");
			put(1004,"TASK_ERR_SEARCH");
			put(1005,"TASK_ERR_OPEN_APP");
			put(1006,"TASK_ERR_PRE_BUY");
			put(1007,"TASK_ERR_PW_SET");
			put(1008,"TASK_ERR_BUY");
			put(1009,"TASK_ERR_BUY_CAP");
			put(1010,"TASK_ERR_DOWNLOAD");
			put(1011,"TASK_ERR_XP");
			put(200,"TASK_ERR_UNKWOWN");
		}
	};

	
	    
	    

}
