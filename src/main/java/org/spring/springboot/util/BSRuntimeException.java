package org.spring.springboot.util;

public class BSRuntimeException extends RuntimeException {

	public BSRuntimeException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -4477883184797763714L;

}
