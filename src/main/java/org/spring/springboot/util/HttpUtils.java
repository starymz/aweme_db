package org.spring.springboot.util;

import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class HttpUtils {
	
	public static String postRequestWithJSON(String url,String body) throws IOException{
		CloseableHttpClient  httpclient = null;
		CloseableHttpResponse response = null;
		try{
			httpclient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(url);
			httpPost.addHeader("Content-type", "application/json; charset=UTF-8");
			// 构建消息实体
	        StringEntity entity = new StringEntity(body, Charset.forName("UTF-8"));
	        entity.setContentEncoding("UTF-8");
	        entity.setContentType("application/json");
			httpPost.setEntity(entity);
	   
			response = httpclient.execute(httpPost);
			HttpEntity respEntity = response.getEntity();
			String responseContent = EntityUtils.toString(respEntity,"UTF-8");
			return responseContent;
		}finally{
			response.close();
			httpclient.close();
		}
	}

}
