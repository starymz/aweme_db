package org.spring.springboot.service;

import java.util.Date;
import java.util.List;

import org.spring.springboot.bo.AwemeCommentBo;
import org.spring.springboot.bo.AwemeURLModelBo;
import org.spring.springboot.bo.AwemeUserBo;
import org.spring.springboot.dao.AwemeCommentMapper;
import org.spring.springboot.dao.AwemeUrlMapper;
import org.spring.springboot.dao.AwemeUserMapper;
import org.spring.springboot.dao.UserFolloweRelMapper;
import org.spring.springboot.domain.AwemeComment;
import org.spring.springboot.domain.AwemeUrl;
import org.spring.springboot.domain.AwemeUser;
import org.spring.springboot.domain.IndexRange;
import org.spring.springboot.domain.UserFolloweRel;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Service
public class AwemeCommentService {
	
	public static int DEFAULT_COUNT = 100;
	
	public static long UPDATE_MIN_TIME = 1000 * 60 * 60;
	
	@Autowired
	private AwemeUserMapper awemeUserMapper;
	
	@Autowired
	private AwemeUrlMapper awemeUrlMapper;
	@Autowired
	private AwemeCommentMapper awemeCommentMapper;
	
	@Autowired
	private UserFolloweRelMapper userFollowRelMapper;

	@Transactional
	public void addComment(AwemeCommentBo comment){
		AwemeComment dbcomment = awemeCommentMapper.selectByCID(comment.getCid());
		
		if(dbcomment == null) {
			AwemeUserBo user = comment.getUser();
			AwemeUser awemeUser = awemeUserMapper.selectByUID(user.getUid());
			AwemeURLModelBo avatarThumb  = user.getAvatar_thumb();
			if(awemeUser == null){
				if(avatarThumb != null && StringUtils.hasText(avatarThumb.getUri())) {
					AwemeUrl awemeUrl = awemeUrlMapper.selectByURI(avatarThumb.getUri());
					if(awemeUrl == null) {
						AwemeUrl url = new AwemeUrl();
						url.setUri(avatarThumb.getUri());
						url.setUrl1(avatarThumb.getUrl_list().get(0));
						url.setUrl2(avatarThumb.getUrl_list().get(1));
						url.setUrl3(avatarThumb.getUrl_list().get(2));
						awemeUrlMapper.insert(url);
					}
				}
				awemeUser = new AwemeUser();
				BeanUtils.copyProperties(user, awemeUser);
				awemeUser.setLast_update_time(new Date());
				if(avatarThumb !=null) {
					awemeUser.setAvatar_thumb(avatarThumb.getUri());
				}
				awemeUserMapper.insert(awemeUser);
			}else if(needUpdate(awemeUser)){
				BeanUtils.copyProperties(user, awemeUser);
				awemeUser.setLast_update_time(new Date());
				if(avatarThumb !=null) {
					awemeUser.setAvatar_thumb(avatarThumb.getUri());
				}
				awemeUserMapper.updateByPrimaryKey(awemeUser);
			}
			
			AwemeComment awemeComment = new AwemeComment();
			BeanUtils.copyProperties(comment, awemeComment);
			awemeComment.setUid(awemeUser.getUid());
			awemeCommentMapper.insert(awemeComment);
		}
	}
	
	private boolean needUpdate(AwemeUser user) {
		if(user == null) {
			return false;
		}
		if(user.getLast_update_time() == null) {
			return true;
		}
		
		if(System.currentTimeMillis() - user.getLast_update_time().getTime() > UPDATE_MIN_TIME ) {
			return true;
		}
		
		return false;
	}
	
	@Transactional
	public IndexRange getAndUpdateFollowIndex(String uid,int count) {
		
		if(count < 0) {
			count = DEFAULT_COUNT;
		}
		
		IndexRange range = new IndexRange();
		
		UserFolloweRel userStatus = userFollowRelMapper.selectByUID(uid);
		if(userStatus == null) {
			range.setStartId(0);
			range.setEndIndex(count);
			
			userStatus = new UserFolloweRel();
			userStatus.setUid(uid);
			userStatus.setNext_follow_id(count);
			userFollowRelMapper.insertSelective(userStatus);
		}else {
			Integer startId = userStatus.getNext_follow_id();
			Integer endId = startId + count;
			range.setStartId(startId);
			range.setEndIndex(endId);
			userStatus.setNext_follow_id(endId);
			userFollowRelMapper.updateByPrimaryKey(userStatus);
		}
		
		return range;
	}
	
	
	@Transactional
	public IndexRange getAndUpdateMessageIndex(String uid,int count) {
		
		if(count < 0) {
			count = DEFAULT_COUNT;
		}
		
		IndexRange range = new IndexRange();
		
		UserFolloweRel userStatus = userFollowRelMapper.selectByUID(uid);
		if(userStatus == null) {
			range.setStartId(0);
			range.setEndIndex(count);
			
			userStatus = new UserFolloweRel();
			userStatus.setUid(uid);
			userStatus.setNext_message_id(count);
			userFollowRelMapper.insertSelective(userStatus);
		}else {
			Integer startId = userStatus.getNext_message_id();
			Integer endId = startId + count;
			range.setStartId(startId);
			range.setEndIndex(endId);
			userStatus.setNext_message_id(endId);
			userFollowRelMapper.updateByPrimaryKey(userStatus);
		}
		
		return range;
	}
	
	
	public List<AwemeUser> queryUserInRange(IndexRange range) {
		return awemeUserMapper.selectUserInRange(range);
	}

}
