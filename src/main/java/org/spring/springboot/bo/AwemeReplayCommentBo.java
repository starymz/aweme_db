package org.spring.springboot.bo;

public class AwemeReplayCommentBo {
	private String cid;
	private int status;
	private String replay_id;
	private String text;
	private long create_time;
	private String aweme_id;
	private int user_digged;
	private int digg_count;
	private AwemeUserBo user;
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getReplay_id() {
		return replay_id;
	}
	public void setReplay_id(String replay_id) {
		this.replay_id = replay_id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public long getCreate_time() {
		return create_time;
	}
	public void setCreate_time(long create_time) {
		this.create_time = create_time;
	}
	public String getAweme_id() {
		return aweme_id;
	}
	public void setAweme_id(String aweme_id) {
		this.aweme_id = aweme_id;
	}
	public int getUser_digged() {
		return user_digged;
	}
	public void setUser_digged(int user_digged) {
		this.user_digged = user_digged;
	}
	public int getDigg_count() {
		return digg_count;
	}
	public void setDigg_count(int digg_count) {
		this.digg_count = digg_count;
	}
	public AwemeUserBo getUser() {
		return user;
	}
	public void setUser(AwemeUserBo user) {
		this.user = user;
	}
}
