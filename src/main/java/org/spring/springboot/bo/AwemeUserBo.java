package org.spring.springboot.bo;

import java.util.List;

public class AwemeUserBo {
	private String district;
	private boolean show_image_bubble;
	private String nickname;
	private String account_region;
	private boolean live_commerce;
	private String uid;
	private AwemeURLModelBo avatar_larger;
	private boolean has_orders;
	private int tw_expire_time;
	private String youtube_channel_id;
	private String weibo_url;
	private int dongtai_count;
	private boolean is_flowcard_member;
	private int favoriting_count;
	private boolean user_canceled;
	private int show_gender_strategy;
	private boolean is_verified;
	private String recommend_reason_relation;
	private AwemeURLModelBo avatar_medium;
	private String signature;
	private String  google_account;
	private String location;
	private int realname_verify_status;
	private int shield_comment_notice;
	private boolean is_phone_binded;
	private boolean is_ad_fake;
	private String language;
	private String province;
	private String weibo_verify;
	private String twitter_id;
	private boolean has_twitter_token;
	private int live_verify;
	private int story_count;
	private String iso_country_code;
	private String region;
	private int shield_follow_notice;
	private boolean accept_private_policy;
	private int school_type;
	private boolean hide_search;
	private int total_favorited;
	private int secret;
	private boolean has_youtube_token;
	private String short_id;
	private int live_agreement;
	private String weibo_name;
	private String custom_verify;
	private long unique_id_modify_time; 
	private int commerce_user_level;
	private String school_name;
	private String birthday;
	private int authority_status;
	private int special_lock;
	private String twitter_name;
	private boolean is_gov_media_vip;
	private String verify_info;
	private int gender;
	private String enroll_year;
	private int following_count;
	private String weibo_schema;
	private String college_name;
	private int login_platform;
	private AwemeURLModelBo avatar_thumb;
	private String city;
	private int aweme_count;
	private String school_poi_id;
	private String unique_id;
	private String bind_phone;
	private String ins_id;
	private List<AwemeFollowerBo> followers_detail;
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public boolean isShow_image_bubble() {
		return show_image_bubble;
	}
	public void setShow_image_bubble(boolean show_image_bubble) {
		this.show_image_bubble = show_image_bubble;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getAccount_region() {
		return account_region;
	}
	public void setAccount_region(String account_region) {
		this.account_region = account_region;
	}
	public boolean isLive_commerce() {
		return live_commerce;
	}
	public void setLive_commerce(boolean live_commerce) {
		this.live_commerce = live_commerce;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public AwemeURLModelBo getAvatar_larger() {
		return avatar_larger;
	}
	public void setAvatar_larger(AwemeURLModelBo avatar_larger) {
		this.avatar_larger = avatar_larger;
	}
	public boolean isHas_orders() {
		return has_orders;
	}
	public void setHas_orders(boolean has_orders) {
		this.has_orders = has_orders;
	}
	public int getTw_expire_time() {
		return tw_expire_time;
	}
	public void setTw_expire_time(int tw_expire_time) {
		this.tw_expire_time = tw_expire_time;
	}
	public String getYoutube_channel_id() {
		return youtube_channel_id;
	}
	public void setYoutube_channel_id(String youtube_channel_id) {
		this.youtube_channel_id = youtube_channel_id;
	}
	public String getWeibo_url() {
		return weibo_url;
	}
	public void setWeibo_url(String weibo_url) {
		this.weibo_url = weibo_url;
	}
	public int getDongtai_count() {
		return dongtai_count;
	}
	public void setDongtai_count(int dongtai_count) {
		this.dongtai_count = dongtai_count;
	}
	public boolean isIs_flowcard_member() {
		return is_flowcard_member;
	}
	public void setIs_flowcard_member(boolean is_flowcard_member) {
		this.is_flowcard_member = is_flowcard_member;
	}
	public int getFavoriting_count() {
		return favoriting_count;
	}
	public void setFavoriting_count(int favoriting_count) {
		this.favoriting_count = favoriting_count;
	}
	public boolean isUser_canceled() {
		return user_canceled;
	}
	public void setUser_canceled(boolean user_canceled) {
		this.user_canceled = user_canceled;
	}
	public int getShow_gender_strategy() {
		return show_gender_strategy;
	}
	public void setShow_gender_strategy(int show_gender_strategy) {
		this.show_gender_strategy = show_gender_strategy;
	}
	public boolean isIs_verified() {
		return is_verified;
	}
	public void setIs_verified(boolean is_verified) {
		this.is_verified = is_verified;
	}
	public String getRecommend_reason_relation() {
		return recommend_reason_relation;
	}
	public void setRecommend_reason_relation(String recommend_reason_relation) {
		this.recommend_reason_relation = recommend_reason_relation;
	}
	public AwemeURLModelBo getAvatar_medium() {
		return avatar_medium;
	}
	public void setAvatar_medium(AwemeURLModelBo avatar_medium) {
		this.avatar_medium = avatar_medium;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public String getGoogle_account() {
		return google_account;
	}
	public void setGoogle_account(String google_account) {
		this.google_account = google_account;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public int getRealname_verify_status() {
		return realname_verify_status;
	}
	public void setRealname_verify_status(int realname_verify_status) {
		this.realname_verify_status = realname_verify_status;
	}
	public int getShield_comment_notice() {
		return shield_comment_notice;
	}
	public void setShield_comment_notice(int shield_comment_notice) {
		this.shield_comment_notice = shield_comment_notice;
	}
	public boolean isIs_phone_binded() {
		return is_phone_binded;
	}
	public void setIs_phone_binded(boolean is_phone_binded) {
		this.is_phone_binded = is_phone_binded;
	}
	public boolean isIs_ad_fake() {
		return is_ad_fake;
	}
	public void setIs_ad_fake(boolean is_ad_fake) {
		this.is_ad_fake = is_ad_fake;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getWeibo_verify() {
		return weibo_verify;
	}
	public void setWeibo_verify(String weibo_verify) {
		this.weibo_verify = weibo_verify;
	}
	public String getTwitter_id() {
		return twitter_id;
	}
	public void setTwitter_id(String twitter_id) {
		this.twitter_id = twitter_id;
	}
	public boolean isHas_twitter_token() {
		return has_twitter_token;
	}
	public void setHas_twitter_token(boolean has_twitter_token) {
		this.has_twitter_token = has_twitter_token;
	}
	public int getLive_verify() {
		return live_verify;
	}
	public void setLive_verify(int live_verify) {
		this.live_verify = live_verify;
	}
	public int getStory_count() {
		return story_count;
	}
	public void setStory_count(int story_count) {
		this.story_count = story_count;
	}
	public String getIso_country_code() {
		return iso_country_code;
	}
	public void setIso_country_code(String iso_country_code) {
		this.iso_country_code = iso_country_code;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public int getShield_follow_notice() {
		return shield_follow_notice;
	}
	public void setShield_follow_notice(int shield_follow_notice) {
		this.shield_follow_notice = shield_follow_notice;
	}
	public boolean isAccept_private_policy() {
		return accept_private_policy;
	}
	public void setAccept_private_policy(boolean accept_private_policy) {
		this.accept_private_policy = accept_private_policy;
	}
	public int getSchool_type() {
		return school_type;
	}
	public void setSchool_type(int school_type) {
		this.school_type = school_type;
	}
	public boolean isHide_search() {
		return hide_search;
	}
	public void setHide_search(boolean hide_search) {
		this.hide_search = hide_search;
	}
	public int getTotal_favorited() {
		return total_favorited;
	}
	public void setTotal_favorited(int total_favorited) {
		this.total_favorited = total_favorited;
	}
	public int getSecret() {
		return secret;
	}
	public void setSecret(int secret) {
		this.secret = secret;
	}
	public boolean isHas_youtube_token() {
		return has_youtube_token;
	}
	public void setHas_youtube_token(boolean has_youtube_token) {
		this.has_youtube_token = has_youtube_token;
	}
	public String getShort_id() {
		return short_id;
	}
	public void setShort_id(String short_id) {
		this.short_id = short_id;
	}
	public int getLive_agreement() {
		return live_agreement;
	}
	public void setLive_agreement(int live_agreement) {
		this.live_agreement = live_agreement;
	}
	public String getWeibo_name() {
		return weibo_name;
	}
	public void setWeibo_name(String weibo_name) {
		this.weibo_name = weibo_name;
	}
	public String getCustom_verify() {
		return custom_verify;
	}
	public void setCustom_verify(String custom_verify) {
		this.custom_verify = custom_verify;
	}
	public long getUnique_id_modify_time() {
		return unique_id_modify_time;
	}
	public void setUnique_id_modify_time(long unique_id_modify_time) {
		this.unique_id_modify_time = unique_id_modify_time;
	}
	public int getCommerce_user_level() {
		return commerce_user_level;
	}
	public void setCommerce_user_level(int commerce_user_level) {
		this.commerce_user_level = commerce_user_level;
	}
	public String getSchool_name() {
		return school_name;
	}
	public void setSchool_name(String school_name) {
		this.school_name = school_name;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public int getAuthority_status() {
		return authority_status;
	}
	public void setAuthority_status(int authority_status) {
		this.authority_status = authority_status;
	}
	public int getSpecial_lock() {
		return special_lock;
	}
	public void setSpecial_lock(int special_lock) {
		this.special_lock = special_lock;
	}
	public String getTwitter_name() {
		return twitter_name;
	}
	public void setTwitter_name(String twitter_name) {
		this.twitter_name = twitter_name;
	}
	public boolean isIs_gov_media_vip() {
		return is_gov_media_vip;
	}
	public void setIs_gov_media_vip(boolean is_gov_media_vip) {
		this.is_gov_media_vip = is_gov_media_vip;
	}
	public String getVerify_info() {
		return verify_info;
	}
	public void setVerify_info(String verify_info) {
		this.verify_info = verify_info;
	}
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	public String getEnroll_year() {
		return enroll_year;
	}
	public void setEnroll_year(String enroll_year) {
		this.enroll_year = enroll_year;
	}
	public int getFollowing_count() {
		return following_count;
	}
	public void setFollowing_count(int following_count) {
		this.following_count = following_count;
	}
	public String getWeibo_schema() {
		return weibo_schema;
	}
	public void setWeibo_schema(String weibo_schema) {
		this.weibo_schema = weibo_schema;
	}
	public String getCollege_name() {
		return college_name;
	}
	public void setCollege_name(String college_name) {
		this.college_name = college_name;
	}
	public int getLogin_platform() {
		return login_platform;
	}
	public void setLogin_platform(int login_platform) {
		this.login_platform = login_platform;
	}
	public AwemeURLModelBo getAvatar_thumb() {
		return avatar_thumb;
	}
	public void setAvatar_thumb(AwemeURLModelBo avatar_thumb) {
		this.avatar_thumb = avatar_thumb;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public int getAweme_count() {
		return aweme_count;
	}
	public void setAweme_count(int aweme_count) {
		this.aweme_count = aweme_count;
	}
	public String getSchool_poi_id() {
		return school_poi_id;
	}
	public void setSchool_poi_id(String school_poi_id) {
		this.school_poi_id = school_poi_id;
	}
	public String getUnique_id() {
		return unique_id;
	}
	public void setUnique_id(String unique_id) {
		this.unique_id = unique_id;
	}
	public String getBind_phone() {
		return bind_phone;
	}
	public void setBind_phone(String bind_phone) {
		this.bind_phone = bind_phone;
	}
	public String getIns_id() {
		return ins_id;
	}
	public void setIns_id(String ins_id) {
		this.ins_id = ins_id;
	}
	public List<AwemeFollowerBo> getFollowers_detail() {
		return followers_detail;
	}
	public void setFollowers_detail(List<AwemeFollowerBo> followers_detail) {
		this.followers_detail = followers_detail;
	}	
}
