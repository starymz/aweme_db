package org.spring.springboot.bo;

import java.util.List;

public class AwemeCommentBo {
	private int status;
	private AwemeUserBo user;
	private String reply_id;
	private String text;
	private long create_time;
	private int digg_count;
	private int user_digged;
	private String aweme_id;
	private String cid;
	
	private List<AwemeReplayCommentBo> reply_comment;
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public AwemeUserBo getUser() {
		return user;
	}
	public void setUser(AwemeUserBo user) {
		this.user = user;
	}
	public String getReply_id() {
		return reply_id;
	}
	public void setReply_id(String reply_id) {
		this.reply_id = reply_id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public long getCreate_time() {
		return create_time;
	}
	public void setCreate_time(long create_time) {
		this.create_time = create_time;
	}
	public int getDigg_count() {
		return digg_count;
	}
	public void setDigg_count(int digg_count) {
		this.digg_count = digg_count;
	}
	public int getUser_digged() {
		return user_digged;
	}
	public void setUser_digged(int user_digged) {
		this.user_digged = user_digged;
	}
	public String getAweme_id() {
		return aweme_id;
	}
	public void setAweme_id(String aweme_id) {
		this.aweme_id = aweme_id;
	}
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public List<AwemeReplayCommentBo> getReply_comment() {
		return reply_comment;
	}
	public void setReply_comment(List<AwemeReplayCommentBo> reply_comment) {
		this.reply_comment = reply_comment;
	}
	
	
}
