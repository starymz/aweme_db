package org.spring.springboot.bo;

import java.util.List;

public class AwemeCommentsResponse {
	private int cursor;
	private int status_code;
	private long _AME_APICommonParam_Timestamp;
	private String _AME_Header_RequestID;
	private int total;
	private int has_more;
	private List<AwemeCommentBo> comments;
	public int getCursor() {
		return cursor;
	}
	public void setCursor(int cursor) {
		this.cursor = cursor;
	}
	public int getStatus_code() {
		return status_code;
	}
	public void setStatus_code(int status_code) {
		this.status_code = status_code;
	}
	public long get_AME_APICommonParam_Timestamp() {
		return _AME_APICommonParam_Timestamp;
	}
	public void set_AME_APICommonParam_Timestamp(long _AME_APICommonParam_Timestamp) {
		this._AME_APICommonParam_Timestamp = _AME_APICommonParam_Timestamp;
	}
	public String get_AME_Header_RequestID() {
		return _AME_Header_RequestID;
	}
	public void set_AME_Header_RequestID(String _AME_Header_RequestID) {
		this._AME_Header_RequestID = _AME_Header_RequestID;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public int getHas_more() {
		return has_more;
	}
	public void setHas_more(int has_more) {
		this.has_more = has_more;
	}
	public List<AwemeCommentBo> getComments() {
		return comments;
	}
	public void setComments(List<AwemeCommentBo> comments) {
		this.comments = comments;
	}
}
