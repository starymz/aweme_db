package org.spring.springboot.bo;

public class BaseOutBo {
	
	private Integer res;
	private Object message;
	public Integer getRes() {
		return res;
	}
	public void setRes(Integer res) {
		this.res = res;
	}
	public Object getMessage() {
		return message;
	}
	public void setMessage(Object message) {
		this.message = message;
	}
	
	public BaseOutBo(){
		res = 0;
		message = "请求成功";
	}
}
