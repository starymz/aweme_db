package org.spring.springboot.bo;

public class AwemeFollowerBo {
	private String icon;
	private String open_url;
	private String download_url;
	private String apple_id;
	private int fans_count;
	private String package_name;
	private String app_name;
	private String name;
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getOpen_url() {
		return open_url;
	}
	public void setOpen_url(String open_url) {
		this.open_url = open_url;
	}
	public String getDownload_url() {
		return download_url;
	}
	public void setDownload_url(String download_url) {
		this.download_url = download_url;
	}
	public String getApple_id() {
		return apple_id;
	}
	public void setApple_id(String apple_id) {
		this.apple_id = apple_id;
	}
	public int getFans_count() {
		return fans_count;
	}
	public void setFans_count(int fans_count) {
		this.fans_count = fans_count;
	}
	public String getPackage_name() {
		return package_name;
	}
	public void setPackage_name(String package_name) {
		this.package_name = package_name;
	}
	public String getApp_name() {
		return app_name;
	}
	public void setApp_name(String app_name) {
		this.app_name = app_name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	

}
