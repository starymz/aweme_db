#!/bin/sh
APP_NAME=cloud-data

tpid=`ps -ef|grep $APP_NAME|grep -v grep|grep -v kill|awk '{print $2}'`
if [ ${tpid} ]; then
        echo 'cloud-data App is running.'
else
        echo 'cloud-data NOT running.'
fi