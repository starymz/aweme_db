create database appaso default charset utf8mb4 COLLATE utf8mb4_unicode_ci;

create user 'appaso'@'%' IDENTIFIED by 'twgdh#2015'; 

grant all on appaso.* to 'appaso'@'%';


create user 'appaso'@'localhost' IDENTIFIED by 'twgdh#2015'; 

grant all on appaso.* to 'appaso'@'localhost';

GRANT SELECT ON mysql.proc TO 'appaso'@'localhost';
GRANT SELECT ON mysql.proc TO 'appaso'@'%';
FLUSH PRIVILEGES;

create table aweme_comment(
	id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
	cid varchar(20) NOT NULL,
	uid varchar(20) NOT NULL,
	replay_id varchar(20),
	text varchar(500),
	create_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	aweme_id varchar(20) NOT NULL,
	status int(10),
	digg_count int(10),
	user_digged int(10),
	PRIMARY KEY (`id`),
	unique index aweme_comment_u1 (cid)
) engine = InnoDb DEFAULT CHARSET=utf8mb4;



create table aweme_user(
  id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  uid varchar(20) NOT NULL,
  nickname varchar(150) DEFAULT '',
  signature varchar(300),
  short_id varchar(20),
  avatar_thumb varchar(40),
  gender int(2),
  birthday varchar(14),
  school_name varchar(200),
  location varchar(50),
  country varchar(100),
  province varchar(100),
  city varchar(100),
  district varchar(100),
  weibo_url varchar(100),
  weibo_verify varchar(100),
  weibo_name varchar(100),
  weibo_schema varchar(100),
  last_update_time timestamp DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`) ,
	unique index aweme_user_u1 (uid)
) engine = InnoDb DEFAULT CHARSET=utf8mb4;


create table aweme_url(
	id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  	uri varchar(40) NOT NULL,
  	url1 varchar(300),
  	url2 varchar(300),	
  	url3 varchar(300),
	PRIMARY KEY (`id`),
	unique index aweme_url_u1 (uri)
) engine = InnoDb DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `user_followe_rel`;
CREATE TABLE `user_followe_rel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uid` varchar(20) NOT NULL,
  `nickname` varchar(150) DEFAULT '',
  `signature` varchar(300) DEFAULT NULL,
  `short_id` varchar(20) DEFAULT NULL,
  `next_follow_id` int(10) unsigned DEFAULT 0,
  `next_message_id` int(10) unsigned DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `aweme_user_u1` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;